CREACION DE USUARIO
POST
URL: {{host}}/users

{
	"firstName":"Federico",
	"lastName":"Baccino",
	"email":"fbaccino@gmail.com",
	"password":"hola1234",
	"crear_cuentas":true
}______________________________________________________________________________________________________________________________________
LOGIN
POST
URL: {{host}}/users

{
	"email":"fbaccino@gmail.com",
	"password":"hola1234"
}
HEADERS RESPONSE:
jwt 
______________________________________________________________________________________________________________________________________
LISTAR USUARIOS
GET
URL: {{host}}/users

HEADERS REQUEST:
Authorization (Debe enviarse el jwt que devolvío el LOGIN))
___________________________________________________________________________________________________________________________________
MODIFICAR USUARIO
PUT
URL: {{host}}/users/{id}
{
	"password":"abcd1234"
}

HEADERS REQUEST:
Authorization (Debe enviarse el jwt que devolvío el LOGIN))
___________________________________________________________________________________________________________________________________
BORRAR  USUARIO
URL: {{host}}/users/{id}
{
	"password":"abcd1234"
}

HEADERS REQUEST:
Authorization (Debe enviarse el jwt que devolvío el LOGIN))
______________________________________________________________________________________________________________________________________
CREACION DE CUENTA
POST
URL: {{host}}/users/id/accounts

{
	"nombre":"Mi Cuenta",
	"moneda":"UYU"
}
_______________________________________________________________________________________________________________________________________
LISTAR CUENTAS
GET
URL: {{host}}/users/id/accounts

HEADERS REQUEST:
Authorization (Debe enviarse el jwt que devolvío el LOGIN))
___________________________________________________________________________________________________________________________________
MODIFICAR CUENTAS
PUT
URL: {{host}}/users/{id}/accounts/{id_accounts}
{
	"nombre":"nuevo nombre cuenta"
}

HEADERS REQUEST:
Authorization (Debe enviarse el jwt que devolvío el LOGIN))
___________________________________________________________________________________________________________________________________
BORRAR  CUENTA
DELETE
URL: {{host}}/users/{id}/accounts/{id_accounts}
{
	"password":"abcd1234"
}

HEADERS REQUEST:
Authorization (Debe enviarse el jwt que devolvío el LOGIN))
________________________________________________________________________________________________________________________________________
CREACION DE TRANSACCION
POST
URL: {{host}}/users/id/accounts/id_account/transactions

{
	"concepto":"Pago de Sueldo",
	"operativa":"TRANSFERENCIA",
	"email_beneficiario":"fbaccino@gmail.com",
	"cuenta_destino":"CTA-52-USD-2,
	"monto":2500
}

"operativa" puede ser: "TRANSFERENCIA","DEPOSITO" o "RETIRO"
"email_beneficiario" y "cuenta_destino" solo aplican a TRANSFERENCIA y son excluyentes va uno u otro

HEADERS REQUEST:
Authorization (Debe enviarse el jwt que devolvío el LOGIN))
_______________________________________________________________________________________________________________________________________
LISTAR TRANSACCIONES
GET
URL: {{host}}/users/id/accounts/id/transactions

HEADERS REQUEST:
Authorization (Debe enviarse el jwt que devolvío el LOGIN))
___________________________________________________________________________________________________________________________________
LOGOUT
DELETE
URL {{host}}/login
HEADERS REQUEST:
Authorization (Debe enviarse el jwt que devolvío el LOGIN))
___________________________________________________________________________________________________________________________________


