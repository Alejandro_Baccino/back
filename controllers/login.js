/**Configuración */
var config = require('../config.js');
//URL del api MLab
var baseMLabURL = config.baseMLabURL; //'https://api.mlab.com/api/1/databases/techubduruguay/';
//api Key del usuario MLab
var apiKey = config.apiKey; //'apiKey=aGkVpuk06S-C6_DxgNiZdbsySpZa4QVd';
var bodyParser = require('body-parser'); //Móudlo para parseo del body del los request
var requestJSON = require('request-json'); //Cliente http para comunicación con el api MLab
var bcrypt = require('bcrypt'); //Módulo de encriptación de passwords
var jwt = require('jsonwebtoken');
var validador = require('../util/validaciones.js');
var listaNegraJWT = require('../model/ListaNegraJWT.js');
var RespuestaAPI = require('../model/respuestaAPI.js');
var User = require('../model/user.js');;

/**
* Controlador para manejar las peticiones al recurso LOGIN. Se espera recibir en el body del request
* un objeto que contiene el email y el password del user que esta pretendiendo autenticarse.
* Se valida que el mail y el password tengan el formato adecuado. Si el formato esta OK, se valida que el usuario
* exista y que la password sea correcta. Si los datos son correctos se responde con
*/
function post(req, res) {
  let login = {
    "email": req.body.email,
    "password": req.body.password
  }
  let respuesta = new RespuestaAPI();
  resultadoValidaciones = validador.validarDatosLogin(login);
  if (!resultadoValidaciones.ok) {
    res.status(400)
    res.send(resultadoValidaciones.errores);
  } else { //Estan los datos necesarios, se a validar contra el MongoDB
    let httpClient = requestJSON.createClient(baseMLabURL);
    let endpointUser = 'collections/user?';
    let q = {
      "email": login.email
    };
    let queryStringMail = 'q=' + JSON.stringify(q) + '&';
    httpClient.get(endpointUser + queryStringMail + apiKey, // Verifico que exista un usuario con el mail
      function(err, respuestaMLab, body) {
        if (!err) {
          let user = new User();
          if (body.length > 0) {
            let passwordEnBBDD = body[0].password;
            user.setProperties(body[0]);
            var passOK = bcrypt.compareSync(login.password, passwordEnBBDD);
            if (passOK) { //Genero el token que se envira en los header de la respuesta;
              let nombre = user.getLastName() + ', ' + user.getFirstName();
              let email = user.getEmail();
              let id = Number(user.getId());
              let objUserToken = {
                "id":id,
                "email":email,
                "nombre":nombre
              }
              var token = jwt.sign(objUserToken, config.jwtClave);
              res.set('jwt', token);
              res.status(200);
              respuesta.setOk(true);
              respuesta.setData(user);
              res.send(respuesta);
            } else {
              res.status(401);
              respuesta.setOk(false);
              respuesta.addError("Credenciales No Válidas")
              res.send(respuesta);
            }
          } else {
            res.status(401);
            respuesta.setOk(false);
            respuesta.addError("Credenciales No Válidas")
            res.send(respuesta);
          }
        } else { // Error accediendo a MongoDB
          res.status(500);
          respuesta.setOk(false);
          respuesta.addError("Error de acceso a base de datos");
          res.send(respuesta);
        }
      });
  }
}

function logout(req, res) {
  token = req.get('Authorization');
  listaNegraJWT.push(token); /*Se envía el token a la lista negra para que no pueda volver a utlizarse */
  res.status(200);
  respuesta.setOK(true);
  respuesta.setMensaje("Logout realizado con éxito");
  res.send(respuesta);
}



module.exports.post = post;
module.exports.logout = logout;
