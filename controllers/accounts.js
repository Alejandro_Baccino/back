/*Configuración */
var config = require('../config.js');
//URL del api MLab
var baseMLabURL = config.baseMLabURL; //'https://api.mlab.com/api/1/databases/techubduruguay/';
//api Key del usuario MLab
var apiKey = config.apiKey; //'apiKey=aGkVpuk06S-C6_DxgNiZdbsySpZa4QVd';
var bodyParser = require('body-parser'); //Móudlo para parseo del body del los request
var requestJSON = require('request-json'); //Cliente http para comunicación con el api MLab
var Account = require('../model/account.js');
var User = require('../model/user.js');
var RespuestaAPI = require('../model/respuestaAPI.js');

/** Contolador del métodO POST de user/:id/accounts
 *   Las cuentas (accounts) se almacenan en MongoDB como un array de objetos accounts.
 *   Las propiedas obligatorias son:
 *   - nombre
 *   - moneda
 *   - saldo (opcional, si existe se valida que sea numérico, valor por defecto 0)
 *   El user "dueño" de las cuentas lleva registro del último número de cuenta asignado a
 *   a efectos de asignar el número de id las nuevas cuentas.
 * @module controllers
 * @function
 * @param {Object} req - Express request object
 * @param {Object} res - Express response object
 */
function post(req, res) {
  let account = new Account();
  account.setProperties(req.body);
  let resultadoValidacion = account.validarDatos(); //Se validan datos recibidos
  let respuesta = new RespuestaAPI();
  // Datos Correctos
  //-- Recupero el usuario
  let httpClient = requestJSON.createClient(baseMLabURL);
  let id = req.params.id;
  let queryString = 'q={"id":' + id + '}&';
  let endpoint = 'collections/user?';
  // Invocación para obtener el usuario de MongoDB
  httpClient.get(endpoint + queryString + apiKey,
    function(err, respuestaMLab, body) {
      if (!err) {
        usuarioOK = body.length > 0;
        if (usuarioOK && resultadoValidacion.ok) { // El usuario existe y los datos eran correctos
          let user = new User();
          user.setProperties(body[0])
          user.incUltNroCuenta();
          let idAccount = 'CTA-' + id + '-' + account.moneda + '-' + user.getUltNroCuenta();
          account.setId(idAccount);
          let accounts = user.getAccounts();
          accounts.push(account);
          var camposSet = {
            "ultNroCuenta": user.getUltNroCuenta(),
            "accounts": accounts
          };
          var objset = {
            "$set": camposSet
          };
          httpClient.put(endpoint + queryString + apiKey, objset,
            function(err, respuestaMLab, body) {
              if (!err) {
                respuesta.setOK(true);
                res.status(200);
                res.send(respuesta);
              } else { //Error al acceder a MongoDB
                respuesta.setOK(false);
                respuesta.addError("Error de acceso a base de datos");
                res.status(500);
                res.send(respuesta);
              }
            });
        } else { // No existe un usuario con el id recibido y/o hay datos incorrectos
          respuesta.setOK(false);
          if (!resultadoValidacion.ok) {
            respuesta.setErrores(resultadoValidacion.getErrores());
          }
          if (!usuarioOK) {
            respuesta.addError("Usuario no encontrado");
          }
          res.status(404);
          res.send(respuesta);
        }
      } else { // !err, error en acceso a MLab buscando usuarios
        respuesta.setOK(false);
        respuesta.addError("Error de acceso a base de datos");
        res.status(500);
        res.send(respuesta);
      }
    });

}

/** Controlador de los método GET de user/:id/accounts y user/:id/accounts/:id_account
 *   Lista todas las cuentas que posee el user
 * @module controllers
 * @function
 * @param {Object} req - Express request object
 * @param {Object} res - Express response object
 */
function get(req, res) {
  let httpClient = requestJSON.createClient(baseMLabURL);
  let id = req.params.id;
  let id_account = req.params.id_account;
  // Construyo el queryFilter excluir el _id de MongoDB y el password
  let queryString = 'q={"id":' + id + '}&';
  let queryFilter = 'f={transactions":0}&';
  let endpoint = 'collections/user?';
  // Invocación para obtener el usuario de MongoDB
  httpClient.get(endpoint + queryString + queryFilter + apiKey,
    function(err, respuestaMLab, body) {
      let respuesta = new RespuestaAPI();
      if (!err) {
        if (body.length > 0) { // El usuario existe y se recuperó
          user = new User();
          user.setProperties(body[0]);
          let fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
          let accHATEOAS ;
          let accounts = user.getAccounts(); //Recupero las cuentas
          if (id_account == undefined) { //se invocó para el path /user/:id/accounts/
            res.status(200);
            respuesta.setOK(true);
            let accountsHATEOAS = [];
            /** Se agregan links HATEOAS */
            for(let i=0; i<accounts.length;i++){
              accHATEOAS = new Account();
              accHATEOAS.setProperties(accounts[i]);
              accHATEOAS.addLinksHATEOAS(fullUrl);
              accountsHATEOAS.push(accHATEOAS);
            }
            respuesta.setData(accountsHATEOAS);
          } else { // se invocó para el path /user/:id/accounts/id_account
            account = user.getAccountById(id_account);
            if (account == undefined) {
              res.status(404);
              respuesta.setOK = true;
              respuesta.addError("Cuenta no encontrada");
            } else {
              res.status(200);
              respuesta.setOK(true);
              /** Se agregan links HATEOAS */
              accHATEOAS = new Account();
              accHATEOAS.setProperties(account);
              fullUrl = fullUrl ;
              accHATEOAS.addLinksHATEOAS(fullUrl);
              respuesta.setData(accHATEOAS);
            }
          }
          res.send(respuesta);
        } else { // No existe un usuario con el id recibido
          res.status(404);
          respuesta.setOK(false);
          respuesta.addError("Usuario no encontrado");
          res.send(respuesta);
        }
      } else { // !err, error en acceso a MLab buscando usuarios
        res.status(500);
        respuesta.addError("Error de acceso a base de datos");
        respuesta.setOK(false);
        res.send(respuesta);
      }
    });
}

/** Controlador del método DELETE para el path user/:id/accounts/:id_account
 *   Borra de la lista de cuentas del usuario aquella cuyo id es id_account
 * @module controllers
 * @function
 * @param {Object} req - Express request object
 * @param {Object} res - Express response object
 */
function deleteAccount(req, res) {
  let httpClient = requestJSON.createClient(baseMLabURL);
  let id = req.params.id;
  let id_account = req.params.id_account;
  // Construyo el queryFilter excluir el _id de MongoDB y el password
  let queryString = 'q={"id":' + id + '}&';
  let queryFilter = 'f={transactions":0}&';
  let endpoint = 'collections/user?';
  // Invocación para obtener el usuario de MongoDB
  httpClient.get(endpoint + queryString + queryFilter + apiKey,
    function(err, respuestaMLab, body) {
      let respuesta = new RespuestaAPI();
      if (!err) {
        if (body.length > 0) { // El usuario existe y se recuperó
          user = new User();
          user.setProperties(body[0]);
          let accounts = user.getAccounts(); //Recupero las cuentas
          let indice = user.getAccountIndex(id_account); // Busco el indice de la cuenta en el array de accounts
          if (indice == undefined) {
            res.status(404)
            respuesta.setOK(false)
            respuesta.addError("Cuenta no encontrada");
            res.send(respuesta);
          } else {
            accounts = user.deleteAccounbByIndex(indice) // Borro la cuenta del array
            var camposSet = { // Acutalizo el user con el nuevo array de cuentas
              "accounts": accounts
            };
            var objset = {
              "$set": camposSet
            };
            httpClient.put(endpoint + queryString + apiKey, objset,
              function(err, respuestaMLab, body) {
                if (!err) {
                  respuesta.setOK(true);
                  res.status(200);
                  res.send(respuesta);
                } else { //Error al acceder a MongoDB
                  respuesta.setOK(false);
                  respuesta.addError("Error de acceso a base de datos");
                  res.status(500);
                  res.send(respuesta);
                }
              });
          }
        } else { // No existe un usuario con el id recibido
          res.status(404);
          respuesta.setOK(false);
          respuesta.addError("Usuario no encontrado");
          res.send(respuesta);
        }
      } else { // !err, error en acceso a MLab buscando usuarios
        res.status(500);
        respuesta.addError("Error de acceso a base de datos");
        respuesta.setOK(false);
        res.send(respuesta);
      }
    });
}

/** Controlador del método PUT para el el path user/:id/accounts/:id_account
 *   Permite modificar el nombre. El saldo no es modificable por que no seria una buena
 *   práctia contable, los saldos se modifican haciendo transaciones sobre la cuenta. Tampoco
 *   se permite modificar la moneda por que tendría efectos contables no deseados.
 * @module controllers
 * @function
 * @param {Object} req - Express request object
 * @param {Object} res - Express response object
 */
function putAccount(req, res) {
  let httpClient = requestJSON.createClient(baseMLabURL);
  let id = req.params.id;
  let id_account = req.params.id_account;
  // Construyo un objeto account con los datos a modificar
  accDatosRecibidos = new Account();
  accDatosRecibidos.setProperties(req.body);
  let respuesta = new RespuestaAPI();
  if (accDatosRecibidos.getMoneda() != undefined) {
    respuesta.setOK(false);
    respuesta.addError("La moneda de la cuenta no es un dato modificable");
  }
  if (req.body.saldo != undefined) {
    respuesta.setOK(false);
    respuesta.addError("El saldo de la cuenta no es un dato modificable");
  }
  if (accDatosRecibidos.getNombre() == undefined) {
    respuesta.setOK(false);
    respuesta.addError("No indicó el nombre de la cuenta (único dato que se puede modficar)");
  }
  if (respuesta.ok == false ) {
    res.status(404);
    res.send(respuesta);
  } else {
    // Construyo el queryFilter excluir el _id de MongoDB y el password
    let queryString = 'q={"id":' + id + '}&';
    let queryFilter = 'f={transactions":0}&';
    let endpoint = 'collections/user?';
    // Invocación para obtener el usuario de MongoDB
    httpClient.get(endpoint + queryString + queryFilter + apiKey,
      function(err, respuestaMLab, body) {
        if (!err) {
          if (body.length > 0) { // El usuario existe y se recuperó
            user = new User();
            user.setProperties(body[0]);
            let accounts = user.getAccounts(); //Recupero las cuentas
            let indice = user.getAccountIndex(id_account); // Busco el indice de la cuenta en el array de accounts
            if (indice == undefined) {
              res.status(404)
              respuesta.setOK(false)
              respuesta.addError("Cuenta no encontrada");
              res.send(respuesta);
            } else {
              acc = accounts[indice]; // Determno el indice de la cuenta en array              console.log(JSON.stringify(acc));
              acc.nombre=accDatosRecibidos.getNombre();
              user.setAccountByIndex(indice,acc);
              var camposSet = { // Acutalizo el user con el nuevo array de cuentas
                "accounts": accounts
              };
              var objset = {
                "$set": camposSet
              };
              httpClient.put(endpoint + queryString + apiKey, objset,
                function(err, respuestaMLab, body) {
                  if (!err) {
                    respuesta.setOK(true);
                    res.status(200);
                    res.send(respuesta);
                  } else { //Error al acceder a MongoDB
                    respuesta.setOK(false);
                    respuesta.addError("Error de acceso a base de datos");
                    res.status(500);
                    res.send(respuesta);
                  }
                });
            }
          } else { // No existe un usuario con el id recibido
            res.status(404);
            respuesta.setOK(false);
            respuesta.addError("Usuario no encontrado");
            res.send(respuesta);
          }
        } else { // !err, error en acceso a MLab buscando usuarios
          res.status(500);
          respuesta.addError("Error de acceso a base de datos");
          respuesta.setOK(false);
          res.send(respuesta);
        }
      });
  }

}

module.exports.post = post;
module.exports.get = get;
module.exports.deleteAccount = deleteAccount;
module.exports.putAccount = putAccount;
