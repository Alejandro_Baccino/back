var config = require('../config.js');
var RespuestaAPI = require('../model/respuestaAPI.js');
var jwt = require('jsonwebtoken');
const URI = config.URI; // '/api-uruguay/v1/';


/** Middelware que procesa tods los request/response
*/
function middelware(req, res, next) {



  respuesta = new RespuestaAPI();  // Respuesta estándar

  console.log(req.method);



  /** Verifico si el PATH es a /LOGIN, si no lo es verifico que venga el JWT y sea válido */
  let pathLogin = URI + 'login';
  let pathUsers = URI + 'users';

  /* Verfico si es la descarga de pdf, a los efectos de la demo le quito la seguridad */
  let largoPath = req.originalUrl.length;
  let terminacion = req.originalUrl.substr(largoPath-3,3);
  let esPdf = false;
  if(terminacion == 'pdf'){
    esPdf = true;
  }

  if( !( req.method == 'POST' && ( req.originalUrl == pathLogin || req.originalUrl == pathUsers)) && !esPdf && !(req.method == 'OPTIONS') && false)  {
      let token = req.get('Authorization'); // obtengo el token jwt
      if(token==undefined){
          res.status(401);
          respuesta.setOk(false);
          respuesta.addError('Acceso no autorizado, por favor autentifíquese');
          res.send(respuesta);
      }else{
        try{
          /** Verfico que el token no este en la lista negra de tokens descartados en el logout */
          let enListaNegra = false
          for(let i = 0; i < listaNegraJWT.length && enListaNegra; i++){
            if(listaNegraJWT[i] == token){
              enListaNegra = true;
            }
          }
          if(!enListaNegra){
            jwt.verify(token,config.jwtClave,function(err,decode){
              if(err){
                res.status(401);
                respuesta.setOk(false);
                respuesta.addError('Error en la verificación del token de seguridad');
                res.send(respuesta);
              }else{
                next();
              }
            });
          }else{
            res.status(401);
            respuesta.setOk(false);
            respuesta.addError('Token de seguridad inválido');
            res.send(respuesta);
          }
        }catch(errorjwt){
          res.status(401);
          respuesta.setOk(false);
          respuesta.addError('No se pudo validar token JWT, autentíquese para generar un nuevo token');
          res.send(respuesta);
        }
      }
  }else{
    if(req.method == 'OPTIONS'){
      console.log("Me voy por OPTIONS");
      res.status(200);
      respuesta.setOk(true);
      res.send(respuesta);
    }else{
      next();
    }
  }
}

module.exports.middleware = middelware;