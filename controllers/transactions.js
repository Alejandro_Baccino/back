/**Configuración */
var config = require('../config.js');
//URL del api MLab
var baseMLabURL = config.baseMLabURL; //'https://api.mlab.com/api/1/databases/techubduruguay/';
//api Key del usuario MLab
var apiKey = config.apiKey; //'apiKey=aGkVpuk06S-C6_DxgNiZdbsySpZa4QVd';
var bodyParser = require('body-parser'); //Móudlo para parseo del body del los request
var requestJSON = require('request-json'); //Cliente http para comunicación con el api MLab

var Account = require('../model/account.js');
var User = require('../model/user.js');
var RespuestaAPI = require('../model/respuestaAPI.js');
var Transaction = require('../model/transaction.js');
var pdf = require('../util/comprobantes.js');
var fs = require('fs');

/** Controlador de los método GET de user/:id/accounts/:id_account/transactions y
 *  user/:id/accounts/:id_account/transactions/:numero
 *  Lista todas las cuentas que posee el user
 */
function get(req, res) {
  let httpClient = requestJSON.createClient(baseMLabURL);
  let id = req.params.id;
  let id_account = req.params.id_account;
  let numero = req.params.numero;
  // Construyo el queryFilter
  let queryString = 'q={"id":' + id + '}&';
  let endpoint = 'collections/user?';
  // Invocación para obtener el usuario de MongoDB
  httpClient.get(endpoint + queryString + apiKey,
    function(err, respuestaMLab, body) {
      let respuesta = new RespuestaAPI();
      if (!err) {
        if (body.length > 0) { // El usuario existe y se recuperó
          user = new User();
          user.setProperties(body[0]);
          let fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
          let accHATEOAS;
          let accounts = user.getAccounts(); //Recupero las cuentas
          let acc;
          let transaAux;
          for (let i = 0;
            (i < accounts.length) && (acc == undefined); i++) { //Busco la cuenta
            if (accounts[i].id == id_account) {
              acc = new Account();
              acc.setProperties(accounts[i]);
            }
          }
          if (acc == undefined) {
            res.status(404);
            respuesta.set(false);
            respuesta.addError("No se encontró la cuenta")
            res.send(respuesta);
          } else {
            transactions = acc.getTransactions();
            if (transactions == undefined) {
              transactions = [];
            }
            transactionsAux = [];
            /** Agregó link HAT */
            let terminar = false;
            for (let j = 0; j < transactions.length && !terminar; j++) {
              if (numero == undefined) { //Retorna todas las transacciones
                transaAux = new Transaction();
                transaAux.setProperties(transactions[j]);
                fullUrl = fullUrl + '/' + transaAux.getNumero();
                transaAux.addLinksHATEOAS(fullUrl);
                transactionsAux.push(transaAux);
              } else { //Retorna la transaccion de la que se le paso el número de transacción
                if (numero == transactions[j].numero) {
                  transaAux = new Transaction();
                  transaAux.setProperties(transactions[j]);
                  transaAux.addLinksHATEOAS(fullUrl);
                  terminar = true;
                }
              }
            }
            if (numero != undefined) {
              if (transaAux != undefined) {
                res.status(200);
                respuesta.setOK(true);
                respuesta.setData(transaAux);
                res.send(respuesta);
              } else {
                res.status(404)
                respuesta.setOK(false);
                respuesta.addError("Transacción no encontrada");
                res.send(respuesta);
              }
            } else {
              res.status(200);
              respuesta.setOK(true);
              respuesta.setData(transactionsAux);
              res.send(respuesta);
            }
          }
        } else { // No existe un usuario con el id recibido
          res.status(404);
          respuesta.setOK(false);
          respuesta.addError("Usuario no encontrado");
          res.send(respuesta);
        }
      } else { // !err, error en acceso a MLab buscando usuarios
        res.status(500);
        respuesta.addError("Error de acceso a base de datos");
        respuesta.setOK(false);
        res.send(respuesta);
      }
    });
}

function operacionNoValida(req, res) {
  let respuesta = new RespuestaAPI();
  respuesta.setOK(false);
  respuesta.addError("Por integridad contable no se permite ni eliminar ni modifcar transacciones. " +
    "Si quiere anular una transacción, genere una equivalente de signo contrario.");
  res.status(405);
  res.send(respuesta);

}

function post(req, res) {
  let respuesta = new RespuestaAPI();
  let httpClient = requestJSON.createClient(baseMLabURL);
  let id = req.params.id; //Id de User
  let id_account = req.params.id_account; //Id de Account
  let transaction = new Transaction();
  transaction.setProperties(req.body);
  transaction.setTimestamp(new Date());
  if (transaction.getOperativa() != 'TRANSFERENCIA' && transaction.getEmail_beneficiario() != undefined) {
    delete transaction.email_beneficiario;
  }
  if (transaction.getOperativa() != 'TRANSFERENCIA' && transaction.getCuenta_destino() != undefined) {
    delete transaction.cuenta_destino;
  }
  let resultadoValidacion = transaction.validarDatos();
  if (resultadoValidacion.getOK()) {
    let queryString = 'q={"id":' + id + '}&';
    let endpoint = 'collections/user?';
    // Invocación para obtener el usuario de MongoDB
    httpClient.get(endpoint + queryString + apiKey,
      function(err, respuestaMLab, body) {
        if (!err) {
          if (body.length > 0) { // El usuario existe y se recuperó
            user = new User();
            user.setProperties(body[0]);
            let accounts = user.getAccounts(); //Recupero cuentas del usuario
            if (accounts != undefined && accounts.length > 0 && user.getAccountById(id_account) != undefined) {
              let acc = new Account();
              acc.setProperties(user.getAccountById(id_account));
              var moneda = acc.getMoneda();
              let pos = user.getAccountIndex(id_account); //posicion de la cuenta en el array
              let saldoOK;
              if (transaction.getOperativa() == 'RETIRO' || transaction.getOperativa() == 'TRANSFERENCIA') {
                if (transaction.getMonto() > acc.getSaldo()) { //Fondos insuficientes
                  res.status(400);
                  respuesta.setOK(false);
                  respuesta.addError("Fondos insuficientes");
                  res.send(respuesta);
                } else {
                  acc.setSaldo(acc.getSaldo() - Number(transaction.getMonto())); //actualizo
                }
              } else { //transaction.operativa ='DEPOSITO'
                acc.setSaldo(acc.getSaldo() + Number(transaction.getMonto())); //actualizo;
              }
              acc.incUltNroMovimiento();
              transaction.setNumero(acc.getUltNroMovimiento());
              acc.addTransaction(transaction);
              accounts[pos] = acc;
              if (transaction.getOperativa() == 'TRANSFERENCIA') {
                let queryBeneficiario;
                //Verifico si se paso email o cuenta beneficiario.
                if (transaction.getEmail_beneficiario() != undefined) {
                  let q = {
                    "email": transaction.getEmail_beneficiario()
                  };
                  queryBeneficiario = 'q=' + JSON.stringify(q) + '&';;
                } else if (transaction.getCuenta_destino() != undefined) {
                  let ctaBenef = transaction.getCuenta_destino();
                  let aux = ctaBenef.substr(4);
                  let posi = aux.indexOf
                  idBenef = aux.substr(0, posi);
                  let objq = {
                    "id": Number(idBenef)
                  };
                  queryBeneficiario = 'q=' + JSON.stringify(objq) + '&';
                }
                httpClient.get(endpoint + queryBeneficiario + apiKey,
                  function(err, respuestaMLab, body) {
                    if (!err) {
                      if (body.length > 0) { // El usuario  beneficiario existe y se recuperó
                        userBene = new User();
                        userBene.setProperties(body[0]);
                        let accountsBene = userBene.getAccounts();
                        let cuentaBene = new Account();
                        let posCtaBene;
                        if (transaction.getCuenta_destino() != undefined) { // Se había indicado cuenta destino
                          cuentaBene.setProperties(userBene.getAccountById((transaction.getCuenta_destino())));
                          posCtaBene = userBene.getAccountIndex(transaction.getCuenta_destino());
                        } else { // Se había indicado email, busco una cuenta en la misma moneda que la cuenta origen
                          let encontreAcc = false;
                          for (let i = 0; i < accountsBene.length && !encontreAcc; i++) {
                            accAux = new Account();
                            accAux.setProperties(accountsBene[i]);
                            if (acc.getMoneda() == accAux.getMoneda()) {
                              encontreAcc = true;
                              posCtaBene = i;
                              cuentaBene = accAux;
                            }
                          }
                        }
                        if (cuentaBene != undefined) { // Se pudo determinar cuenta destino

                          cuentaBene.incUltNroMovimiento();
                          trnBen = new Transaction(); // se crea transaccion para la cuenta beneficiaria
                          trnBen.setMonto(transaction.getMonto());
                          trnBen.setConcepto(transaction.getConcepto());
                          trnBen.setOperativa('TRANSFERENCIA RECIBIDA');
                          trnBen.setTipo('CREDITO');
                          trnBen.setDepositante(user.getFirstName() + ', ' + user.getLastName());
                          trnBen.setTimestamp(new Date());
                          trnBen.setNumero(cuentaBene.getUltNroMovimiento());
                          cuentaBene.addTransaction(trnBen);
                          cuentaBene.setSaldo(cuentaBene.getSaldo() + transaction.getMonto());
                          //Actualizo cuenta beneficiario
                          accountsBene[posCtaBene] = cuentaBene;
                          var camposSet = {
                            "accounts": accountsBene
                          };
                          var objset = {
                            "$set": camposSet
                          };
                          httpClient.put(endpoint + queryBeneficiario + apiKey, objset,
                            function(err, respuestaMLab, body) {
                              if (!err) {
                                //Actualizo cuenta origen
                                camposSet = {
                                  "accounts": accounts
                                };
                                objset = {
                                  "$set": camposSet
                                };
                                httpClient.put(endpoint + queryString + apiKey, objset,
                                  function(err, respuestaMLab, body) {
                                    if (!err) {
                                      /*Se genera comprobante PDF */
                                      let datos = {
                                        "cliente": user.getLastName() + ', ' + user.getFirstName(),
                                        "moneda": acc.getMoneda(),
                                        "cuenta": acc.getNombre(),
                                        "monto": transaction.getMonto(),
                                        "beneficiario": userBene.getLastName() + ', ' + userBene.getFirstName(),
                                        "numero": transaction.getNumero()
                                      }
                                      /* Se contruye el nombre del archivo user_account_numero */
                                      ;
                                      let nombreArchivo = user.getId() + '-' + acc.getId() + '-' + transaction.getNumero();
                                      pdf.crear(datos, nombreArchivo); // Función que genera el pdf.
                                      res.status(200);
                                      respuesta.setOk(true);
                                      respuesta.setMensaje("Transferencia realizada, número: " + transaction.getNumero());
                                      res.send(respuesta);
                                    } else { //Error al acceder a MongoDB
                                      res.status(500);
                                      respuesta.setOk(false);
                                      respuesta.addError("Error de acceso a base de datos");
                                      res.send(respuesta);
                                    }
                                  });
                              } else { //Error al acceder a MongoDB
                                res.status(500);
                                respuesta.setOk(false);
                                respuesta.addError("Error de acceso a base de datos");
                                res.send(respuesta);
                              }
                            });
                        } else { // No se pudo determinar cuenta destino
                          respuesta.setOK(false);
                          respuesta.addError("El beneficiario no posee cuenta en " + moneda);
                          res.status(400);
                          res.send(respuesta);
                        } //
                      } else { //No se encontró el usuario beneficiario
                        respuesta.setOK(false);
                        respuesta.addError("No se encontró el benficiario de la transferencia");
                        res.status(404);
                        res.send(respuesta);
                      }
                    } else { // !err, error en acceso a MLab buscando usuarios
                      respuesta.setOK(false);
                      respuesta.addError("Error de acceso a base de datos");
                      res.status(500);
                      res.send(respuesta);
                    }
                  });
              } else { //RETIRO O DEPOSITO
                //Actualizo cuenta origen
                camposSet = {
                  "accounts": accounts
                };
                objset = {
                  "$set": camposSet
                };
                httpClient.put(endpoint + queryString + apiKey, objset,
                  function(err, respuestaMLab, body) {
                    if (!err) {
                      res.status(200);
                      respuesta.setOk(true);
                      res.send(respuesta);
                    } else { //Error al acceder a MongoDB
                      res.status(500);
                      respuesta.setOk(false);
                      respuesta.addError("Error de acceso a base de datos");
                      res.send(respuesta);
                    }
                  });
              }
            } else {
              res.status(404);
              respuesta.setOK(false);
              respuesta.addError("Cuenta no encontrada");
              res.send(respuesta); //No encontre cuenta
            }
          } else { // No existe un usuario con el id recibido
            res.status(404);
            respuesta.setOK(false);
            respuesta.addError("Usuario no encontrado");
            res.send(respuesta);
          }
        } else { //Error acceso a datos en búsqueda de user
          respuesta.setOK(false);
          respuesta.addError("Error de acceso a base de datos");
          res.status(500);
          res.send(respuesta);
        }
      }); //Fin del httpClient para buscar el user
  } else {
    //Datos no válidos
    res.status(400);
    respuesta.setOk(false);
    respuesta.setErrores(resultadoValidacion.getErrores());
    res.send(respuesta);
  }
}

function archivo(req, res) {
  let user = req.params.id;
  let account = req.params.id_account;
  let numero = req.params.numero;
  let nombreArchivo = user + '-' + account + '-' + numero + '.pdf';
  let filePath = './comprobantes/' + nombreArchivo;;
  fs.readFile(filePath,
    function(err, data) {
      res.contentType("application/pdf");
      res.send(data);
    });
}

module.exports.post = post;
module.exports.get = get;
module.exports.operacionNoValida = operacionNoValida;
module.exports.archivo = archivo;
