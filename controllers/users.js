/**Configuración */
var config = require('../config.js');
//URL del api MLab
var baseMLabURL = config.baseMLabURL; //'https://api.mlab.com/api/1/databases/techubduruguay/';
//api Key del usuario MLab
var apiKey = config.apiKey; //'apiKey=aGkVpuk06S-C6_DxgNiZdbsySpZa4QVd';
var bodyParser = require('body-parser'); //Móudlo para parseo del body del los request
var requestJSON = require('request-json'); //Cliente http para comunicación con el api MLab
var bcrypt = require('bcrypt'); //Módulo de encriptación de passwords
var mailValidator = require('email-validator'); //Módulo de validación de formato de emails


/** Módulo de validaciones de datos */
var validador = require('../util/validaciones.js');
var RespuestaAPI = require('../model/respuestaAPI.js');
var User = require('../model/user.js');
var Account = require('../model/account.js');
var enviarMail = require('../util/mail.js');
var html = require('../util/generaHTML.js');

/**************************/
/* CONTROLADORES          */
/**************************/

/**
Controlador para manejar las peticiones GET al recurso USER
Devuelve todos los users, y en caso de que se especifiquen queryParms devuelve los que
satisfagan los criterios de filtro
Establece una conexión http con MLab y a través del api de MLab accede a la colección "users".
Permite filtros por firstName, lastName e email.
Cuando trae los datos se excluye el password por motivos de seguridad.
También se excluye la colección de accounts asociada  al user, estas deben obtenerse con una petición
a /user/accounts
*/
function get(req, res) {
  let httpClient = requestJSON.createClient(baseMLabURL);
  // Construyo el queryFilter excluir el _id de MongoDB y el password
  let queryFilter = 'f={"_id":0,"password":0,"accounts":0}&';
  // Verifico si vienen query parms y si vienen construo el objeto "q" para la consulta a MongoDB
  let q = {};
  if (req.query.firstName != undefined) {
    q.firstName = req.query.firstName;
  }
  if (req.query.lastName != undefined) {
    q.lastName = req.query.lastName;
  }
  if (req.query.mail != undefined) {
    q.mail = req.query.mail;
  }
  if (q != {}) { //-- Si vino algún query parm lo agrego a la consulta
    queryFilter = queryFilter + 'q=' + JSON.stringify(q) + '&';
  }
  let endpoint = 'collections/user?';
  // Invocación para obtener la lista de usuarios
  httpClient.get(endpoint + queryFilter + apiKey,
    function(err, respuestaMLab, body) {
      let respuesta = new RespuestaAPI();
      if (!err) {
        respuesta.setOK(true);
        let listaUsers = [];
        for(let i = 0; i<body.length;i++){
          let userAux = new User();
          userAux.setProperties(body[i]);
          var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
          userAux.addLinksHATEOAS(fullUrl);
          listaUsers.push(userAux);
        }
        respuesta.setData(listaUsers);
        res.status(200); //OK
        res.send(respuesta); // Se retorna el resultado que devolvió el api de Mlab
      } else { // !err, error en acceso a MLab buscando usuarios
        respuesta.setOK(false);
        respuesta.addError("Error de acceso a base de datos");
        res.status(500);
        res.send(respuesta);
      }
    });
}

/**
Controlador para manejar las peticiones GET al recurso cuyo id coincide con el del path USER/ID.
También se obtiene por el api de MLab. Aplica el mismo criterio que en el GET para passwords y accounts.
*/
function get_id(req, res) {
  let httpClient = requestJSON.createClient(baseMLabURL);
  let id = req.params.id;
  // Construyo el queryFilter excluir el _id de MongoDB y el password
  let queryFilter = 'f={"_id":0,"password":0,"accounts":0}&';
  let queryString = 'q={"id":' + id + '}&';
  let endpoint = 'collections/user?';
  // Invocación para obtener el usuario de MongoDB
  httpClient.get(endpoint + queryString + queryFilter + apiKey,
    function(err, respuestaMLab, body) {
      if (!err) {
        let respuesta = new RespuestaAPI();
        if (body.length > 0) { // El usuario existe y se recuperó
          res.status(200);
          respuesta.setOK(true);
          respuesta.setData(body[0]);
          res.send(respuesta);
        } else { // No existe un usuario con el id recibido
          res.status(404);
          respuesta.setOK(false);
          respuesta.addError("Usuario no encontrado");
          res.send(respuesta);
        }
      } else { // !err, error en acceso a MLab buscando usuarios
        res.status(500);
        respuesta.addError("Error de acceso a base de datos");
        respuesta.setOK(false);
        res.send(respuesta);
      }
    });
}

/**
Controlador para manejar las peticiones POST  para la creación de un recurso user.
Los datos del user a crear vienen en body de parámetro request. Las propiedades esperadas son:
- firstName (obligatoria)
- lastName (obligatoria)
- email (obligatoria e identificador único de usuario, se utilizará en el login)
- password (obligatoria y de largo mínimo 8. Se almacena encriptada en la base de datos)
Si los criterios antes descritos no se cumplen se retorna un error.
Si los datos están ok se crea el usuario en MongoDB mediante el api de MLab.
A los nuevos user se les asinga un id numérico interno que se obtiene de un numerador de entidades que no
es otra cosa que una documento en MongoDB que almacena el último número utilizado.
*/
function post(req, res) {
  //-- Se validan los datos recibidos
  var user = new User();
  user.setProperties(req.body); // Construo usuario con los datos
  let altaCuentas = req.body.crear_cuentas;
  let resultadoValidaciones = user.validarDatos(); // Validación de datos recibidos
  let respuesta = new RespuestaAPI(); // Respuesta a enviar en el body del response
  if (!resultadoValidaciones.ok) {
    respuesta.setOK(false);
    respuesta.setErrores(resultadoValidaciones.getErrores());
    res.status(400);
    res.send(respuesta);
  } else {
    let httpClient = requestJSON.createClient(baseMLabURL);
    // Construyo el queryString para la obtención de id único de usuario
    let entidad = 'user';
    let queryString = '{"entidad:"' + entidad + '}&'
    let endpoint = 'collections/numerador?';
    let numero = 1; // Incializo el numerador en 1 por si no Existe el numerador en MongoDB
    let _id;
    // Invocación para obtener el id a asignar al nuevo usuario
    httpClient.get(endpoint + queryString + apiKey,
      function(err, respuestaMLab, body) {
        if (!err) {
          // Existe el numerador, obtengo el último número utilizado y lo incremento
          if (body.length > 0) {
            numero = body[0].numero + 1;
            _id = body[0]._id.$oid;
          }
          let numerador = { // Actualizo el numerador
            "entidad": entidad,
            "numero": numero
          }
          let endpointUser = 'collections/user?';
          user.setId(numero);
          user.setPassword(user.password) //-- El setPassword asigna y previamente encripta
          if(altaCuentas){ //-- Se indicó que se crearan cuentas por defecto en el alta de usuario
            let accounts = [];
            let ctaUYU = new Account();
            ctaUYU.initAccount(numero, 'UYU', 'Caja de Ahorro UYU', 1);
            accounts.push(ctaUYU);
            let ctaUSD = new Account();
            ctaUSD.initAccount(numero, 'USD', 'Caja de Ahorro USD', 2);
            accounts.push(ctaUSD);
            user.setAccounts(accounts);
            user.setUltNroCuenta(2);
          }
          /** Preparo datos para notificación por mail */
          let texto =  "Estimado/a Sr/Sra " + user.getLastName() + ", "  + user.getFirstName() + " le informamos que ha sido dado de alta como cliente premium en nuestro banco."
          let datosMail = {
            "to":user.email,
            "subject": "BANCO NINJA - Alta de Cuenta",
            "text": html.textoMail(user.getLastName() + ", "  + user.getFirstName())
          }
          if (numero == 1) { // Si no existia el numerador en MongoDB lo doy de alta
            httpClient.post(endpoint + apiKey, numerador, // Creo el numerador
              function(err, respuestaMLab, body) {
                let q = {
                  "email": user.email
                };
                let queryStringMail = 'q=' + JSON.stringify(q) + '&';
                httpClient.get(endpointUser + queryStringMail + apiKey, // Verifico no exista un usuario con el mail
                  function(err, respuestaMLab, body) {
                    if (!err) {
                      if (!body.length > 0) {
                        httpClient.post(endpointUser + apiKey, user, // Creo el nuevo user
                          function(err, respuestaMLab, body) {
                            if (!err) {
                              console.log(datosMail);
                              enviarMail.enviar(datosMail);
                              respuesta.setOK(true);
                              respuesta.setData(user);
                              res.status(200);
                              res.send(respuesta);
                            } else { // !err, error en acceso a MLab buscando numerador
                              respuesta.setOK(false);
                              respuesta.addError("Error de acceso a base de datos");
                              res.status(500);
                              res.send(respuesta);
                            }
                          });
                      } else { //Ya existía una usuario con el mail enviado
                        respuesta.setOK(false);
                        respuesta.addError("Ya existe un usuario con el mail enviado");
                        res.status(400);
                        res.send(respuesta);
                      }
                    }
                  });
              });
          } else { // Si el numerador ya existía en MongoDB lo actualizo incrementando en uno
            queryString = '{"_id:"' + _id + '}&';
            httpClient.put(endpoint + queryString + apiKey, numerador, // Actualizo el numerador
              function(err, respuestaMLab, body) {
                let q = {
                  "email": user.email
                };
                let queryStringMail = 'q=' + JSON.stringify(q) + '&';
                httpClient.get(endpointUser + queryStringMail + apiKey, // Verifico no exista un usuario con el mail
                  function(err, respuestaMLab, body) {
                    if (!err) {
                      if (!body.length > 0) {
                        httpClient.post(endpointUser + apiKey, user, // Creo el nuevo user
                          function(err, respuestaMLab, body) {
                            if (!err) {
                              enviarMail.enviar(datosMail);
                              respuesta.setOK(true);
                              respuesta.setData(user);
                              res.status(200);
                              res.send(respuesta);
                            } else { // !err, error en acceso a MLab buscando numerador
                              respuesta.setOK(false);
                              respuesta.addError("Error de acceso a base de datos");
                              res.status(500);
                              res.send(respuesta);
                            }
                          });
                      } else { //Ya existía una usuario con el mail enviado
                        respuesta.setOK(false);
                        respuesta.addError("Ya existe un usuario con el mail enviado");
                        res.status(400);
                        res.send(respuesta);
                      }
                    }
                  });
              });
          }
        } else { // !err, error en acceso a MLab buscando numerador
          respuesta.setOK(false);
          respuesta.addError("Error de acceso a base de datos");
          res.status(500);
          res.send(respuesta);
        }
      });
  } // fin else de resultadoValidaciones.ok
}

/**
Controlador para manejar peticiones PUT para la modificación de una entidad user existe.
En el path viene el Id interno del user a modficar. Se verifia que el usuario exista y se hacen las
mismas validaciones que en la creación. Si el usuario no existe o no se cumple alguna validación se
retorna un error.
*/
function put_id(req, res) {
  let id = req.params.id;
  //-- Se verifica que en el body venga alguno de los parametros actualizables
  let firstName = req.body.firstName;
  let lastName = req.body.lastName;
  let email = req.body.email;
  let password = req.body.password;
  // En el siguiente If se verifica que al menos uno de los valores modificables (firstName,
  // lastName, email, password) venga en el body y tenga el largo requerido
  if ((firstName != undefined && firstName.length > 0) ||
    (lastName != undefined && lastName.length > 0) ||
    (email != undefined && email.length > 0) ||
    (password != undefined && password.length > 7)) {
    // Campos es un objeto para utilizar el el comando $Set de MongoDB, contiene los campos
    // a modificar y sus valores
    let campos = {};
    if (firstName != undefined && firstName.length > 0) {
      campos.firstName = firstName;
    }
    if (lastName != undefined && lastName.length > 0) {
      campos.lastName = lastName;
    }
    if (email != undefined && email.length > 0) {
      campos.email = email;
    }
    if (password != undefined && password.length > 7) {
      // Se encripta el password antes de presistirlo
      campos.password = bcrypt.hashSync(password, BCRYPT_SALT_ROUNDS)
    }
    let httpClient = requestJSON.createClient(baseMLabURL);
    let objset = {
      "$set": campos
    }; // Cambios a realizar
    let endpoint = 'collections/user?';
    let queryString = 'q={"id":' + id + '}&';
    httpClient.put(endpoint + queryString + apiKey, objset, // Se envía el put con los cambios
      function(err, respuestaMLab, body) {
        let respuesta = new RespuestaAPI(); // Respuesta a enviar en el body del response
        if (!err) {
          if (body.n > 0) {
            res.status(200);
            respuesta.setOK(true);
            res.send(respuesta);
          } else {
            respuesta.setOK(false);
            respuesta.addError("Usuario no encontrado");
            res.status(404);
            res.send(respuesta);
          }

        } else { // El api a MongoDB retorno error
          respuesta.setOK(false);
          respuesta.addError("Error de acceso a base de datos");
          res.status(500);
          res.send(respuesta);
        }
      });
  } else {
    res.status(400);
    if (password != undefined && password.length < 8) {
      respuesta.setOK(false);
      respuesta.addError("El largo del password debe ser 8 o más");
      res.status(404);
      res.send(respuesta);
    } else {
      respuesta.setOK(false);
      respuesta.addError("Parámetros incorrectos");
      res.status(404);
      res.send(respuesta);
    }
  }
}

/** Controlador para manejar peticiones DELETE para la eliminación de un user cuyo id interno
viene en el path de la petición. Se valida que el user existe y si no se retorna error.
Si el user existe se borra con el DELET del api de MLab.
*/
function delete_id(req, res) {
  let httpClient = requestJSON.createClient(baseMLabURL);
  let id = req.params.id;
  // Se recupera el objeto para obtener _id de MongoDB necesario para el DELETE
  let queryFilter = 'f={"password":0}&';
  let queryString = 'q={"id":' + id + '}&';
  let endpoint = 'collections/user?';
  // Invocación para obtener el usuario de MongoDB
  httpClient.get(endpoint + queryString + queryFilter + apiKey,
    function(err, respuestaMLab, body) {
      let respuesta = new RespuestaAPI();
      if (!err) {
        if (body.length > 0) { // El usuario existe y se recuperó
          let _id = body[0]._id.$oid; // _id contiene el identificador interno del documento en MongoDB
          httpClient.delete('collections/user/' + _id + '?' + apiKey,
            function(err, respuestaMLab, body) {
              if (!err) {
                respuesta.setOK(true);
                res.status(200);
                res.send(respuesta);
              } else { // Error en la acceso a MongoDB
                respuesta.setOK(false);
                respuesta.addError("Parámetros incorrectos");
                res.status(550);
                res.send(respuesta);
              }
            });
        } else { // No existe un usuario con el id recibido
          respuesta.setOK(false);
          respuesta.addError("Usuario no encontrado");
          res.status(404);
          res.send(respuesta);
        }
      } else { // !err, error en acceso a MLab buscando usuarios
        res.status(500);
        res.send({
          "mensaje": "Error de acceso a base de datos"
        });
      }
    });
}

module.exports.get = get;
module.exports.get_id = get_id;
module.exports.put_id = put_id;
module.exports.post = post;
module.exports.delete_id = delete_id;
