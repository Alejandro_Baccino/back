# API de Usuarios, Cuentas y Transacciones

Proyecto Node.js y Express que implementa un api para gestión de usuarios, cuentas y transacciones de una entidad financiera.



## Documentación Funcional del API

#### Resumen de métodos.

| RESOURCE                                             | GET (Get) | PUT (Update) | POST (Create) | DELETE (Delete) |
| ---------------------------------------------------- | --------- | ------------ | ------------- | --------------- |
| /users                                               | OK        | OK           | OK            | OK              |
| /users/id                                            | OK        | OK           | OK            | OK              |
| /users/id/accounts                                   | OK        | OK           | OK            | OK              |
| /users/id/accounts/id_account                        | OK        | OK           | OK            | OK              |
| /users/id/accounts/id_account/transactions           | OK        | OK           | OK            | OK              |
| users/id/accounts/id_account/transactions/numero     | OK        | OK           | N/P           | N/P             |
| users/id/accounts/id_account/transactions/numero/pdf | OK        | N/A          | N/A           | N/A             |
| /login                                               | N/A       | N/A          | OK            | OK              |

OK: Método implementado, N/P: No permitido, método impementado que retorna un http status 405, operación no permitida, N/A: No Aplica, método no implementado.

## Prerequisitos

Se requiere el gestor paquetes npm instalado. Para verificar que esta instalado ejecute:

```bash
npm -version
```

Si está correctamente instalado debe desplegarse el número de la versión.

## Instalación

Para instalar la aplicación debe situarse en la carpeta raiz  del proyecto (donde está el archivo package.json) y ejecutar:

```bash
npm install
```

## Ejecución

Para ejecutar la aplicación y dejar levantado el servidor que atienda las peticiones que sirven los recursos del api ejecutar

```shell 
npm start
```

## Estructura de carpetas del proyecto

------

### ./ (raiz) 

**Dockerfile** - Archivo de configuración para empaquetado en contenedor docker.

**README.MD** - Documentación descriptiva del proyecto.

**app.js** - Ejecutable principal de la aplicación. Se ejecuta cuando se lanza el comando npm start.

**config.js** - Variables y Constantes globales al proyecto.

**package.json** - Archivo de configuración npm de la aplicación

------

### controllers

Carpeta que contiene los .js que proveen las funciones callback que implementan el api (users.js, accounts.js, transacctions.js, login.js, middleware.js). Estas funciones callback resuelven  las peticiones de los difrentes métodos http a los recursos del api.

------

### model

El desarrollo de la aplicación hace uso  de posiblidad de utilizar clases y constructores que ofrece ECMAScript 2015. En la carpeta model se encuentran los archivos .js que implementan las clases del modelo de dominio de la aplicación. Los archivos de impementación de clases son:

**user.js**

**account.js**

**transaction.js**

**ListaNegraJWT.js**

**mensajeError.js**

**respuestaAPI.js**

**resultadoValidacion.js**

------

### util

La carpeta util contiene archivos .js de que brindan  funcionalidades auxiliares a la aplicación. Estos archivos son:

- **mail.js** - Utilizado para envío de mails.
- **comprobantes.js** - Para generación de comprobantes pdf.
- **generaHTML.js** - Funciones que generan templates html, para mails y comprobantes.
- **validaciones.js** - Funciones de validación para procesar request http.