var MensajeError = require('./mensajeError.js');

/** Clase utilizada para representar el resultado de la validaciones sobre un objeto recibido.
* tiene una propiedad boolean que esta en true si el objeto aprobó todas las validaciones y en false
* en caso de que haya datos inválidos.
* Para el caso de que no se superen las valiaciones hay un array con la descripciñon de los errores.
*/
class ResultadoValidacion{

  constructor(){
    this.ok = true;
  }

  /** Getter */
  getOK(){
    return this.ok;
  }

  setOK(ok){
    this.ok = ok;
  }

  getErrores(){
    return  this.errores;
  }

  addError(error){
    this.ok = false;
    let msgerror = new MensajeError(error);
    if (this.errores == undefined){
      this.errores = [];
    }
    this.errores.push(msgerror);
  }

  setOk(ok){
      this.ok = ok;
  }

}

module.exports = ResultadoValidacion;
