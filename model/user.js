var bcrypt = require('bcrypt');
var mailValidator = require('email-validator');

var config = require('../config.js');
var ResultadoValidacion = require('./resultadoValidacion.js');

/** Usuauarios */
class User {
  /**
  * Metodo que permite construir un user a partir de otro objeto.
  */
  setProperties(obj){
    this.id = obj.id;
    this.firstName =  obj.firstName;
    this.lastName = obj.lastName;
    this.email = obj.email;
    this.password = obj.password;
    this.accounts = obj.accounts;
    this.ultNroCuenta = obj.ultNroCuenta;
    this.fechaPassword = obj.fechaPassword;
  }

  /** Getter */
  getId(){
    return this.id;
  }

  /** Setter */
  setId(id){
    this.id = id;
  }

  /** Getter */
  getFirstName(){
    return this.firstName;
  }

  /** Setter */
  setFirstName(firstName){
    this.firstName = firstName;
  }

  /** Getter */
  getLastName(){
    return this.lastName;
  }

  /** Setter */
  setLastName(lastName){
    this.lastName = lastName;
  }

  /** Getter */
  getEmail(){
    return this.email;
  }

  /** Setter */
  setEmail(email){
    this.email = email;
  }

  /** Getter */
  getPassword(){
    return this.password;
  }

  /** Setter */
  setPassword(password){
    /** Se encripta el password para su pertistencia */
    this.password  = bcrypt.hashSync(password, config.BCRYPT_SALT_ROUNDS);
  }

  /** Getter */
  getAccounts(){
    if (this.accounts == undefined){
      this.accounts = [];
    }
    return this.accounts;
  }

  /** Setter */
  setAccounts(accounts){
    this.accounts = accounts;
  }

  /** Getter */
  getUltNroCuenta(){
    return this.ultNroCuenta;
  }

  /** Setter */
  setUltNroCuenta(ultNroCuenta){
    this.ultNroCuenta = ultNroCuenta;
  }


  /** Este método incrementa en uno el último número de cuenta utilizado,
  *   Si es nulo lo inicaliza en uno
  */
  incUltNroCuenta(){
    if (this.ultNroCuenta == undefined){
      this.ultNroCuenta = 1;
    }else{
      this.ultNroCuenta++;
    }
  }

  /** Este método agrega una cuenta al usuario */
  addCuenta(account){
    if (this.accounts== undefined){
      this.accounts = [];
    }
    this.accounts.push(account);
  }

  /** Se obtiene la cuenta del user cuyo id se  recibe como parámetro */
  getAccountById(idAccount){
      let ac;
      let fin;
      for( let i=0; i < this.accounts.length && !fin; i++ ){
        if (this.accounts[i].id == idAccount){
          ac = this.accounts[i];
          fin = true;
        }
      }
      return ac;
  }

  /** Permite actulizar la cuenta que esta en la posición (en accounts) que recibe como parámetro.
  *   La cuenta es sutituida por la que se recibe como parámetro.
  */
  setAccountByIndex(index, acc){
    this.accounts[index] = acc;
  }


  /** Se obtiene el indice de la account cuyo id se  recibe como parámetro */
  getAccountIndex(idAccount){
      let indice;
      let fin = false;
      for( let i =0; i < this.accounts.length && !fin; i++ ){
        if (this.accounts[i].id == idAccount){
          indice = i;
        }
      }
      return indice;
  }

  /** Elimina la cuenta de la que se le pasa el indice */
  deleteAccounbByIndex(index){
    this.accounts.splice(index,1);
    return this.accounts;
  }

  addLinksHATEOAS(uri,id){
    if(this.links == undefined){
      this.links = [];
    }
    let self = {
      "rel":"self",
      "href":uri+'/'+ this.id
    };
    this.links.push(self);
    let accountsLink = {
      "rel":"accounts",
      "href":uri+'/'+ this.id+'/accounts'
    }
    this.links.push(accountsLink);
  }


  /** Validación de datos */
  validarDatos(){
    var resultado = new ResultadoValidacion();
    //Valido firstName
    if(this.firstName == undefined){
      resultado.addError("Debe indicar firstName");
    }
    if(this.lastName == undefined){
      resultado.addError("Debe indicar lastName");
    }
    //Valido password
    if (this.password == undefined){
      resultado.addError("Debe indicar password");
    }else if(this.password.length < 8){
      resultado.addError("El largo mínimo del password es 8");
    }
    //Valido email
    if (this.email == undefined){
      resultado.addError("Debe indicar email");
    }else if(!mailValidator.validate(this.email)){
      resultado.addError("Formato de email incorrecto");
    }
    return resultado;
  }

}

module.exports = User;
