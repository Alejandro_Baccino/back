var config = require('../config.js');
var ResultadoValidacion = require('./resultadoValidacion.js');
var validateCurrencyCode = require('validate-currency-code');

/** Account */
class Account {

  setProperties(obj){
    this.id = obj.id;
    this.nombre = obj.nombre;
    this.moneda = obj.moneda;
    this.transactions = obj.transactions;
    if(obj.saldo == undefined){ // Se asinga 0 como valor por defecto al saldo
      this.saldo = 0;
    }else{
      this.saldo = Number(obj.saldo);
    }
    this.ultNroMovimiento = obj.ultNroMovimiento;
  }

  /**Setter id */
  setId(id){
    this.id = id;
  }

  /** Getter id */
  getId(){
    return this.id;
  }

  /**Setter nombre*/
  setNombre(nombre){
    this.nombre = nombre;
  }

  /** Getter nombre */
  getNombre(){
    return this.nombre;
  }

  /**Setter moneda */
  setNombre(moneda){
    this.moneda = moneda;
  }

  /** Getter moneda */
  getMoneda(){
    return this.moneda;
  }

  /**Setter saldo */
  setSaldo(saldo){
    this.saldo = saldo;
  }

  /** Getter saldo */
  getSaldo(){
    if(this.saldo==undefined){
      this.saldo = 0;
    }
    return this.saldo;
  }

  /**Setter ultNroMovimiento */
  setUltNroMovimiento(ultNroMovimiento){
    this.ultNroMovimiento = ultNroMovimiento;
  }

  /** Getter saldo */
  getUltNroMovimiento(){
    return this.ultNroMovimiento;
  }

  /** Incrementa último número de movimiento */
  incUltNroMovimiento(){
    if (this.ultNroMovimiento == undefined){
      this.ultNroMovimiento = 1
    }else {
      this.ultNroMovimiento++;
    }
  }

  /** Getter */
  getTransactions(){
    if(this.transactions == undefined){
      this.transactions = [];
    }
    return this.transactions;
  }

  /** Setter */
  setTransactions(transactions){
    this.transactions = transactions;
  }

  /** Agrega un movimiento */
  addTransaction(transaction){
    if (this.transactions == undefined){
      this.transactions = [];
    }
    this.transactions.push(transaction);
  }

  /** Retorna ID de Cliente propietario de la cuenta */
  getIdCliente(){
    //Elimino los primeros 4 caractéres (CTA-)
    let aux = this.id.substring(4);
    aux = aux.indexOf("-");
    return aux.substring(0,aux);
  }

  initAccount(cliente, moneda, nombre, numero){
    let idCta = 'CTA-' + cliente + '-' + moneda + '-' + numero;
    this.id = idCta;
    this.moneda = moneda;
    this.nombre = nombre;
    this.saldo = 0;
  }

  addLinksHATEOAS(uri){
    if(this.links == undefined){
      this.links = [];
    }
    let self = {
      "rel":"self",
      "href":uri
    };
    this.links.push(self);
    let accountsLink = {
      "rel":"transactions",
      "href":uri+ '7transactions'
    }
    this.links.push(accountsLink);
  }

  /** Valida los datos necesarios para crear una cuenta */
  validarDatos(){
    let resultado = new ResultadoValidacion();
    //Valido nombre
    if (this.nombre == undefined){
      resultado.addError("Debe indicar el nombre de la cuenta");
    }
    //Valido moneda
    if (this.moneda == undefined){
      resultado.addError("Debe indicar la moneda de la cuenta");
    }else if(!validateCurrencyCode(this.moneda)){
      resultado.addError("El código de moneda no es válido");
    }
    if(this.saldo != undefined && isNaN(this.saldo)){
      resultado.addError("El saldo debe ser numérico");
    }
    return resultado;
  }


}

module.exports = Account;
