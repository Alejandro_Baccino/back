var MensajeError = require('./mensajeError.js');

/** Clase generica que encapsulara las respuestas a las peticiones del api */
class RespuestaAPI{

  /** Boolean que indica si hubo error. */
  setOK(ok){
    this.ok = ok;
  }

  setOk(ok){
    this.ok = ok;
  }

  /** Boolean que indica si hubo error. */
  setErrores(errores){
    this.errores = errores;
  }

  /** Datos solicitados */
  setData(data){
    this.data = data;
  }

  /** Setter para Mensaje de respuestas OK */
  setMensaje(mensaje){
    this.mensaje = mensaje;
  }

  /** Mensaje para respuestas OK */
  getMensaje(){
    return this.mensaje;
  }

  /** Agrego un mensaje a la lista de de errores
  * @param error String con el mensaje de error
  */
  addError(error){
    let objError = new MensajeError(error);
    if (this.errores == undefined){
      this.errores = [];
    }
    this.errores.push(objError);
  }

  getOK(){
    return this.ok;
  }

}

module.exports = RespuestaAPI;
