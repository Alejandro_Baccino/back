var ResultadoValidacion = require('./resultadoValidacion.js');
var RespuestaAPI = require('../model/respuestaAPI.js');

/** Trasaction representa un movimiento que afecta el saldo de una cuenta */
class Transaction {

  setProperties(obj) {
    this.monto = obj.monto;
    this.concepto = obj.concepto;
    /** Operativa que genera el movimiento (DEPOSITO, RETIRO, TRANSFERENCIA) */
    this.operativa = obj.operativa;
    /** Tipo de movimiento (DEBITO, CREDITO)*/
    if(obj.operativa == 'DEPOSITO'){
      this.tipo = 'CREDITO';
    }else{
      this.tipo = 'DEBITO';
    }
    /* Para las operativas de TRANSFERENCIA debe especifcarse el destino
     *  puede indicarse el "email" del usuario beneficiario o el id de la cuenta
     *  del beneficiario. Se debe indicar uno u otro.
     */
    this.cuenta_destino = obj.cuenta_destino;
    this.email_beneficiario = obj.email_beneficiario;
    this.numero = obj.numero;
    this.timestamp = obj.timestamp;
  }

  setMonto(monto){
    this.monto=monto;
  }

  getMonto(){
    return this.monto;
  }

  setConcepto(concepto){
    this.concepto = concepto;
  }

  getConcepto(){
    return this.concepto;
  }

  setOperativa(operativa){
    this.operativa = operativa;
  }

  getOperativa(){
    return this.operativa;
  }

  setTipo(tipo){
    this.tipo = tipo;
  }

  getTipo(){
    return this.tipo;
  }

  setCuenta_destino(cuenta){
    this.cuenta_destino = cuenta;
  }

  getCuenta_destino(){
    return this.cuenta_destino;
  }

  setEmail_beneficiario(email){
    this.email_beneficiario = email;
  }

  getEmail_beneficiario(){
    return this.email_beneficiario;
  }

  setNumero(numero){
    this.numero = numero;
  }

  getNumero(){
    return this.numero;
  }

  setTimestamp(timeStamp){
    this.timestamp = timeStamp;
  }

  getTimestamp(){
    return this.timestamp;
  }

  setDepositante(timeStamp){
    this.depositante = timeStamp;
  }

  getDepositante(){
    return this.depositante;
  }

  addLinksHATEOAS(uri){
    if(this.links == undefined){
      this.links = [];
    }
    let self = {
      "rel":"self",
      "href":uri
    };
    this.links.push(self);
  }

  validarDatos() {
    let resultado = new ResultadoValidacion();
    //Valido Monto
    if (this.monto == undefined) {
      resultado.setOk(false);
      resultado.addError("Debe indicar el monto  de la transacción");
    } else if (isNaN(this.monto)) {
      resultado.setOk(false) ;
      resultado.addError("El monto de la transacción debe ser numérico");
    }
    //Valido el Concepto
    if (this.concepto == undefined || (this.tipo.length == 0)) {
      resultado.setOk(false) ;
      resultado.addError("Debe indicar el concepto de la transacción");
    }
    //Valido operativa
    if (this.operativa == undefined || (this.operativa != 'RETIRO' && this.operativa != 'DEPOSITO' && this.operativa != 'TRANSFERENCIA')) {
      resultado.setOk(false) ;
      resultado.addError("Debe indicar la operativa (RETIO, DEPOSITO o TRANSFERENCIA)");
    }else{ //Si la operativa estuvo OK, asingo el tipo (DEBITO o CREDITO)
      if(this.operativa=='DEPOSITO'){ // Los depósitos generan créditos y los retiros y transferencias débitos.
        this.tipo = 'CREDITO'
      }else{
       this.tipo = 'CREDITO'
      }
    }
    //Valido Datos de Cuenta Benficiaria
    if (this.operativa == 'TRANSFERENCIA' && (this.cuenta_destino == undefined && this.email_beneficiario == undefined)) {
      resultado.setOk(false) ;
      resultado.addError("Debe indicar el email o el identificador de la cuenta del beneficiario");
    }
    return resultado;
  }

}

module.exports = Transaction;
