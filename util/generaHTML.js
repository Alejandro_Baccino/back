/** Esta función genera un string que continene un html con el comprobante
*   de una transferencia. El html se construye dinámicamente con los datos que
*   se reciben como parámetro.
*   El html generado se utilizará para la construcción del pdf que puden servirese
*   a través del api.
*/
function genera(datosTransferencia){
  let htmlstr = '<html><head>' +
  '    <title>BANCO NINJA - Comprobante de Transferencia</title>' +
  '    <style> ' +
  '        tr{ ' +
  '            padding: 5px' +
  '        }' +
  '        td{' +
  '            padding: 5px' +
  '        }' +
  '    </style>' +
  '</head>' +
  '<body>' +
  '    <div style="margin: 0 auto;width: 80%;  border:1px solid black " >' +
  '        <div align="center">' +
  '            <h1>NINJA BANK</h1>' +
  '        </div>' +
  '        <div align="center">' +
  '            <h2>Comprobante de Transferencia</h2>' +
  '        </div>' +
  '        <div align="center">' +
  '            <table style="padding: 5px">' +
  '                <tr>' +
  '                    <td>Cliente</td>' +
  '                    <td>' + datosTransferencia.cliente + '</td>' +
  '                </tr>' +
  '                <tr>' +
  '                    <td>Cuenta</td>' +
  '                    <td>' + datosTransferencia.cuenta  +'</td>' +
  '                </tr>' +
  '                <tr>' +
  '                    <td>Número de Movimiento</td>' +
  '                    <td>' + datosTransferencia.numero  +'</td>' +
  '                </tr>' +
  '                <tr>' +
  '                    <td>Moneda</td>' +
  '                    <td>' + datosTransferencia.moneda  +'</td>' +
  '                </tr>' +
  '                <tr>' +
  '                    <td>Monto</td>' +
  '                    <td>' + datosTransferencia.monto  +'</td>' +
  '                </tr>' +
  '                <tr>' +
  '                    <td>Beneficiario</td>' +
  '                    <td>' + datosTransferencia.beneficiario  +'</td>' +
  '                </tr>' +
  '            </table>' +
  '        </div>' +
  '    </div>' +
  '</body>' +
  '</html>';
  return htmlstr;
}

function textoMail(cliente){

  let rethtml = '<html>' +
  '    <head>' +
  '        <meta charset="UTF-8">' +
  '        <title>Mail</title>' +
  '    </head>' +
  '    <body style="color: #000000; font-family: Arial, sans-serif, ‘Open Sans’; font-size: 20px; line-height: 18px">' +
  '        <div>' +
  '        <br>' +
  '        <p>Estimado/a Sr/a. <strong style="font-size: 24px">' + cliente +'</strong> queremos comunicarle que ha sido dado de alta en nuestro ' +
  '           banco como cliente premium.' +
  '        </p>' +
  '        <p>Como gentileza de nuestra parte y a modo de promoción le entregaremos una tarjeta Platinum' +
  '           con una línea de USD 500.000 y el derecho al usufructo vitalicio de un palco en ' +
  '           el Gran Parque Central o en el Campeón del Siglo según usted elija.' +
  '        </p>' +
  '        <p>Lo saluda atentamente la gerencia de NINJA BANK</p>' +
  '       </div>' +
  '    </body>' +
  '</html>';
  return rethtml;
}

module.exports.genera = genera;
module.exports.textoMail = textoMail;
